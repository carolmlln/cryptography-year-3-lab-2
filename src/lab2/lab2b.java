package lab2;

import java.math.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Carol Mullen D00196117
 */
public class lab2b 
{
    public static void main(String[] args) 
    {
        int a = 880, b = 35, c = 19;
        int result = getModPower(a, b, c);
        System.out.println("The result of " + a + " to the power of " + b + " modulo " + c + " is " + result);
        BigInteger quick = modPow("880", "35", "19");
        System.out.println("\nQuick Version using BigInteger.modPow: " + quick);
        System.out.println("\n");
    }
    
    static int getModPower(int a, int b, int c)
    {
        //create arrayLists to store the binary and mod values.
        List<Integer> bin = new ArrayList<>();
        List<Integer> fastMod = new ArrayList<>();
        
        //convert the power to binary
        String x = Integer.toBinaryString(b);
        
        //get the mod of the number to the power of 1
        int ans = getModulo(a,c);
        fastMod.add(ans);//save the answer
        int total = 1; //create a storage variable for the total calculation
        
        //for the whole of the now binary power add to the bin arrayList
        for(int i = 0; i < x.length(); i++)
        {
            bin.add((int)(x.charAt(i)- '0'));
        }
        //reverse the arrayList so that the numbers are being read from left to right in the necessary order
        Collections.reverse(bin);
        
        //for the binary number - calculate the mod of the number a to the power of the binary digit
        for(int i = 0; i <= bin.size()-1; i++)
        {
            if(i > 0)
            {
                ans = getModulo((ans * ans), c);
                fastMod.add(ans); //add the result of each a^bin mod c calculation to the fastMod arrayList
            }
            if(bin.get(i) == 1) //as long as the bin char is 1 do the following caclulation:
            {
                //multiply each of the previous mods together and then mod the answer
                total = getModulo((total * fastMod.get(i)), c);
            }
        }
        
//        System.out.println(fastMod); //print each mod value
//        System.out.println(bin); //print the binary value
        
        return total;
    }
    
    public static int getModulo(int a, int c)
    {
        int remainder;

        if (a >= 0)
        {
            remainder = a- (c*(a/c));
        } 
        else 
        {

            double times = Math.ceil((double) (a*-1)/ (double) c);
            remainder = a+ c*((int) times);
        }

        return (int) remainder;
    }
    //Quicker way to calculate the modPow using BigInteger and the .modPow method
    public static BigInteger modPow(String value, String power, String mod)
    {
        //declare the three bigInteger values
        BigInteger x = new BigInteger(value);
        BigInteger y = new BigInteger(power);
        BigInteger z = new BigInteger(mod);
        //return the modPow
        return x.modPow(y, z);
    }

}
