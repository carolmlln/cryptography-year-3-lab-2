package lab2;
/**
 * @author Carol Mullen D00196117 
  
                            Test values
================================================================================
        a = 4864, n = 3458                        a = 880, n = 19
================================================================================
                            Expected Results
================================================================================
        { d, x, y } = [38, 32, -45]               { d, x, y } = [1, -3, 139]
        The inverse of a does not exist           The inverse of a is: -3
================================================================================ 
 */
import java.math.*;
import java.util.Arrays;
public class Lab2 
{
    public static void main(String[] args) 
    {
        BigInteger a = BigInteger.valueOf(880); //prime number
        BigInteger n = BigInteger.valueOf(19); //world working in (mod n)
        BigInteger[] num = gcd(a, n); //array to hold extended euclidean values
        //print initial values
        System.out.println("a = " + a);
        System.out.println("n = " + n);
        //print array values
        System.out.println("\n{ d, x, y } = " + Arrays.toString(num));
        System.out.println("\n");
        //retrieve d, the gcd
        BigInteger d = num[0];
        //retrieve x, the inverse
        BigInteger x = num[1];
        BigInteger j = BigInteger.valueOf(1);
        //compare d the gcd to the value 1
        int res = d.compareTo(j);
        
        //if d = 1
        if(res == 0)
        System.out.println("The inverse of a is: " + x);
        //if d != 1
        else if(res == 1)
        System.out.println("The inverse of a does not exist");
        else if( res == -1 )
        System.out.println("Error");
        //System.out.println(a.modPow(x, n));

    }
    //method to calculate the extended euclidean
    static BigInteger[] gcd(BigInteger a, BigInteger n)
    {
        if(n == BigInteger.valueOf(0))
            return new BigInteger[] {a, BigInteger.valueOf(1), BigInteger.valueOf(0)};
    
        BigInteger[] num = gcd(n, a.mod(n));
        BigInteger d = num[0];
        BigInteger x = num[2];
        BigInteger y = num[1].subtract((a.divide(n)).multiply(num[2]));
        return new BigInteger[] { d, x, y };
        
    }
    
}
