package lab2;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Carol Mullen D00196117
 */
public class Lab2Total 
{
    public static void main(String[] args) 
    {
        BigInteger a = BigInteger.valueOf(880); //prime number
        BigInteger n = BigInteger.valueOf(19); //world working in (mod n)
        BigInteger[] num = gcd(a, n); //array to hold extended euclidean values
        //print initial values
        System.out.println("Computing multiplicative inverses");
        System.out.println("====================================================");
        System.out.println("a = " + a);
        System.out.println("n = " + n);
        //print array values
        System.out.println("\n{ d, x, y } = " + Arrays.toString(num));
        //retrieve d, the gcd
        BigInteger d = num[0];
        //retrieve x, the inverse
        BigInteger x = num[1];
        BigInteger j = BigInteger.valueOf(1);
        //compare d the gcd to the value 1
        int res = d.compareTo(j);
        
        //if d = 1
        if(res == 0)
        System.out.println("The inverse of a is: " + x);
        //if d != 1
        else if(res == 1)
        System.out.println("The inverse of a does not exist");
        else if( res == -1 )
        System.out.println("Error");
        
        int value = 880, power = 35, modulus = 19;
        int result = getModPower(value, power, modulus);
        System.out.println("\nRepeated square-and-multiply algorithms for exponentiation");
        System.out.println("====================================================");
        System.out.println("a = " + value);
        System.out.println("b = " + power);
        System.out.println("n = " + modulus);
        System.out.println("The result of " + value + " to the power of " + power + " modulo " + modulus + " is " + result);
        BigInteger quick = modPow("880", "35", "19");
        System.out.println("\nQuick Version using BigInteger.modPow: " + quick);
        System.out.println("\n");
        
    }
    
    //method to calculate the extended euclidean
    static BigInteger[] gcd(BigInteger a, BigInteger n)
    {
        if(n == BigInteger.valueOf(0))
            return new BigInteger[] {a, BigInteger.valueOf(1), BigInteger.valueOf(0)};
    
        BigInteger[] num = gcd(n, a.mod(n));
        BigInteger d = num[0];
        BigInteger x = num[2];
        BigInteger y = num[1].subtract((a.divide(n)).multiply(num[2]));
        return new BigInteger[] { d, x, y };
        
    }
    
    static int getModPower(int a, int b, int c)
    {
        //create arrayLists to store the binary and mod values.
        List<Integer> bin = new ArrayList<>();
        List<Integer> fastMod = new ArrayList<>();
        
        //convert the power to binary
        String x = Integer.toBinaryString(b);
        
        //get the mod of the number to the power of 1
        int ans = getModulo(a,c);
        fastMod.add(ans);//save the answer
        int total = 1; //create a storage variable for the total calculation
        
        //for the whole of the now binary power add to the bin arrayList
        for(int i = 0; i < x.length(); i++)
        {
            bin.add((int)(x.charAt(i)- '0'));
        }
        //reverse the arrayList so that the numbers are being read from left to right in the necessary order
        Collections.reverse(bin);
        
        //for the binary number - calculate the mod of the number a to the power of the binary digit
        for(int i = 0; i <= bin.size()-1; i++)
        {
            if(i > 0)
            {
                ans = getModulo((ans * ans), c);
                fastMod.add(ans); //add the result of each a^bin mod c calculation to the fastMod arrayList
            }
            if(bin.get(i) == 1) //as long as the bin char is 1 do the following caclulation:
            {
                //multiply each of the previous mods together and then mod the answer
                total = getModulo((total * fastMod.get(i)), c);
            }
        }
        
//        System.out.println(fastMod); //print each mod value
//        System.out.println(bin); //print the binary value
        
        return total;
    }
    
    public static int getModulo(int a, int c)
    {
        int remainder;

        if (a >= 0)
        {
            remainder = a- (c*(a/c));
        } 
        else 
        {

            double times = Math.ceil((double) (a*-1)/ (double) c);
            remainder = a+ c*((int) times);
        }

        return (int) remainder;
    }
    //Quicker way to calculate the modPow using BigInteger and the .modPow method
    public static BigInteger modPow(String value, String power, String mod)
    {
        //declare the three bigInteger values
        BigInteger x = new BigInteger(value);
        BigInteger y = new BigInteger(power);
        BigInteger z = new BigInteger(mod);
        //return the modPow
        return x.modPow(y, z);
    }
}
