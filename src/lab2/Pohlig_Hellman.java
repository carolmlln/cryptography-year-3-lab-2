package lab2;
import java.math.*;
import java.util.Arrays;
import java.util.Random;
/**
 *
 * @author Carol Mullen D00196117   
 */
public class Pohlig_Hellman 
{
    public static void main(String[] args) 
    {
        BigInteger p = BigInteger.valueOf(883); //an odd prime p
        BigInteger m = BigInteger.valueOf(353); //an integer M such that 0 ≤ M < p
        BigInteger n = p.subtract(BigInteger.ONE); //p-1
        BigInteger e = BigInteger.valueOf(83); //an integer e such that 0, e < p and gcd(e, p − 1) = 1
        BigInteger[] num = gcd(e, n); //array to hold extended euclidean values 
        //System.out.println("{ d, x, y } = " + Arrays.toString(num)); //check that there is an inverse
        //i.e d = 1, x = the inverse
        BigInteger inverse = num[1];
        System.out.println("Prime p: " + p);
        System.out.println("Message m: " + m);

        BigInteger c = new BigInteger("0");
        if(!p.divide(BigInteger.valueOf(2)).equals(BigInteger.ZERO))
        {
            c = m.modPow(e, p); //Encrypt m
            System.out.println("\nEncrypt = " + c);
        }
        else
            System.out.println("Error");
        
        BigInteger d = c.modPow(inverse, p); //Decrypt c
        System.out.println("Decrypt = " + d);
        
        
    }
        //method to calculate the extended euclidean
    static BigInteger[] gcd(BigInteger e, BigInteger n)
    {
        if(n == BigInteger.valueOf(0))
            return new BigInteger[] {e, BigInteger.valueOf(1), BigInteger.valueOf(0)};
    
        BigInteger[] num = gcd(n, e.mod(n));
        BigInteger d = num[0];
        BigInteger x = num[2];
        BigInteger y = num[1].subtract((e.divide(n)).multiply(num[2]));
        return new BigInteger[] { d, x, y };
        
    }
}
